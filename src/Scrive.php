<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Scrive
{
    private static $products;
    private static $ACCESS_TOKEN;

    const STATUS_COMPLETE = 'complete';

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Scrive()->Products();
    }

    private static function getSequenceCode($name = 'products')
    {
        if(self::$$name) {
            return self::$$name;
        }
        else {
            self::selfConstruct();
            return self::getSequenceCode($name);
        }
    }

    private static function executeMethod($methodCode, $alterations, $returnPayload = false)
    {
        $response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
        return array('error' => false, 'response' => json_decode(json_encode($response), true));
    }

    static function getAccessToken()
    {
        if(self::$ACCESS_TOKEN)
        {
            return self::$ACCESS_TOKEN;
        }
        else
        {
            self::$ACCESS_TOKEN = "Bearer " . env('SCRIVE_ACCESS_TOKEN');
            return self::getAccessToken();
        }
    }

    static function finlandAuth($redirectUrl)
    {
        $methodCode = self::getSequenceCode()->providerFitupasAuth()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $redirectUrl
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function norwayAuth($redirectUrl)
    {
        $methodCode = self::getSequenceCode()->providerNoBankIDAuth()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken(),
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $redirectUrl,
            "providerParameters" => [
                "auth" => [
                    "noBankID" => [
                        "requestNNIN" => false,
                        "phoneNumber" => null,//"+4787935834",
                        "personalNumber" => null,//"20042006263",
                        "allowSubstantialLevel" => true
                    ]
                ]
            ],
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function denmarkAuth($redirectUrl)
    {
        $methodCode = self::getSequenceCode()->providerMitIDAuth()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken(),
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $redirectUrl,
            "providerParameters" => [
                "auth" => [
                    "dkMitID" => [
                        "cpr" => null,
                        "employeeLogin" => true,
                        "language" => "En",
                        "level" => "Substantial",
                        "referenceText" => null,
                        "requestCPR" => true
                    ]
                ]
            ],
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function finlandSign($body)
    {
        $methodCode = self::getSequenceCode()->providerFitupasSign()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $body['redirectUrl'],
            'providerParameters' => [
                'sign' => [
                    "fiTupas" => [
                        "personalNumber" => $body['personalNumber'],
                        "signTitle" => $body['signTitle'],
                        "signText" => $body['signText'],
                        "signDescription" => $body['signDescription'],
                    ]
                ]
            ]
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function norwaySign($body)
    {
        $methodCode = self::getSequenceCode()->providerNoBankIDSign()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken(),
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $body['redirectUrl'],
            "providerParameters" => [
                "sign" => [
                    "noBankID" => [
                        "phoneNumber" => null,
                        "personalNumber" => $body['personalNumber'],
                        "signTitle" => $body['signTitle'],
                        "signText" => $body['signText'],
                        "signDescription" => $body['signDescription'],
                    ]
                ]
            ],
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function denmarkSign($body)
    {
        $methodCode = self::getSequenceCode()->providerMitIDSign()->newTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken(),
        );

        $alterations[]['body'] = array(
            'redirectUrl' => $redirectUrl,
            "providerParameters" => [
                "sign" => [
                    "dkMitID" => [
                        "cpr" => null,
                        "employeeLogin" => true,
                        "language" => "En",
                        "level" => "Substantial",
                        "referenceText" => $body['signText'],
                        "requestCPR" => true
                    ]
                ]
            ],
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }

    static function status($transactionId)
    {
        $methodCode = self::getSequenceCode()->flow()->getTransaction();

        $alterations[]['headers'] = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['path'] = array(
            'transaction_id' => $transactionId
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(isset($response['response'])) return $response['response'];
        return $response;
    }
}
